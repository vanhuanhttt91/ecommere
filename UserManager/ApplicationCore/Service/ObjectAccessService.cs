﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Domain;
using Infrastructure.Domain;
using Infrastructure.Repository;
using Infrastructure.RepositoryOrmlite;
using Infrastructure.RepositoryOrmlite.UnitOfWork;

namespace ApplicationCore.Service
{
    public class ObjectAccessService : IObjectAccessService
    {
        private IRepositoryObjectAccess _repositoryObjectAccess ;
        private readonly IRepositoryKvAAShard<ObjectAccess> _repositoryKvAAShard;
        public ObjectAccessService(IRepositoryObjectAccess repositoryObjectAccess, IUnitOfWorkKvShard unitOfWorkKvShard)
        {
            _repositoryObjectAccess = repositoryObjectAccess;
            _repositoryKvAAShard = unitOfWorkKvShard.GetGenericRepository<ObjectAccess>();
        }

        public async Task<List<ObjectAccess>> GetObjectAccessesAsync()
        {
            return await _repositoryKvAAShard.GetAllAsync();
        }

        public async Task<bool> InsertObjectAccessesAsync(List<ObjectAccess> objectAccesses, List<User> users, List<Role> roles)
        {
            return await _repositoryObjectAccess.InsertObjectAccessAsync(objectAccesses, users, roles);
        }
    }
}
