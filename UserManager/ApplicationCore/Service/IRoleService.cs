﻿using Infrastructure.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public interface IRoleService
    {
        Task<bool> InsertRoleAsync(List<Role> roles);
        Task<List<Role>> GetAllRolesAsync();
    }
}
