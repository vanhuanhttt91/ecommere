﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public interface IPropertyAccessService
    {
        Task<List<PropertyAccess>> GetPropertyAccessesAsync();
        Task<bool> InsertPropertyAccessesAsync(List<PropertyAccess> permissions, List<User> users, List<Role> roles);
    }
}