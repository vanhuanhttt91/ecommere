﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public interface IUserService
    {
        Task<bool> InsertUsersAsync(List<User> users, List<Role> roles, List<ObjectAccess> objectAccesses,
            List<PropertyAccess> propertyAccesses, List<Permission> permissions);

        Task<List<User>> GetAllUsersAsync();
    }
}
