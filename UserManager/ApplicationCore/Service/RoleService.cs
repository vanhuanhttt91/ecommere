﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Domain;
using Infrastructure.Repository;
using Infrastructure.RepositoryOrmlite;
using Infrastructure.RepositoryOrmlite.UnitOfWork;

namespace ApplicationCore.Service
{
    public class RoleService : IRoleService
    {
        private IRepositoryRole _repositoryRole;
        private IRepositoryKvAAShard<Role> _repositoryKvAAShardRole;
        public RoleService(IRepositoryRole repositoryRole, IUnitOfWorkKvShard unitOfWorkKvShard)
        {
            _repositoryRole = repositoryRole;
            _repositoryKvAAShardRole = unitOfWorkKvShard.GetGenericRepository<Role>();
        }
        public async Task<List<Role>> GetAllRolesAsync()
        {
            return await _repositoryKvAAShardRole.GetAllAsync();
        }

        public async Task<bool> InsertRoleAsync(List<Role> roles)
        {
            return await _repositoryRole.InsertRolesAsync(roles);
        }
    }
}
