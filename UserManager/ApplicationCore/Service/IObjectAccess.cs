﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public interface IObjectAccessService
    {
        Task<List<ObjectAccess>> GetObjectAccessesAsync();
        Task<bool> InsertObjectAccessesAsync(List<ObjectAccess> objectAccesses, List<User> users, List<Role> roles);
    }
}
