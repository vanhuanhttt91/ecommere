﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using Infrastructure.Repository;
using Infrastructure.RepositoryOrmlite;
using Infrastructure.RepositoryOrmlite.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public class PropertyAccessService : IPropertyAccessService
    {
        private IRepositoryPropertyAccess _repositoryPropertyAccess;
        private readonly IRepositoryKvAAShard<PropertyAccess> _repositoryKvAAShard;
        public PropertyAccessService(IRepositoryPropertyAccess repositoryPropertyAccess, IUnitOfWorkKvShard unitOfWorkKvShard)
        {
            _repositoryPropertyAccess = repositoryPropertyAccess;
            _repositoryKvAAShard = unitOfWorkKvShard.GetGenericRepository<PropertyAccess>();
        }
        public async Task<List<PropertyAccess>> GetPropertyAccessesAsync()
        {
            return await _repositoryKvAAShard.GetAllAsync();
        }

        public async Task<bool> InsertPropertyAccessesAsync(List<PropertyAccess> propertyAccesses, List<User> users, List<Role> roles)
        {
            return await _repositoryPropertyAccess.InsertPropertyAccessesAsync(propertyAccesses, users, roles);
        }
    }
}
