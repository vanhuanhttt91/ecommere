﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using Infrastructure.Repository;
using Infrastructure.RepositoryOrmlite;
using Infrastructure.RepositoryOrmlite.UnitOfWork;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public class UserService : IUserService
    {
        private IRepositoryUser _repositoryUser;
        private readonly IRepositoryKvAAShard<User> _repositoryKvAAShardUser;
        public UserService(IRepositoryUser repositoryUser, IUnitOfWorkKvShard unitOfWorkKvShard)
        {
            _repositoryUser = repositoryUser;
            _repositoryKvAAShardUser = unitOfWorkKvShard.GetGenericRepository<User>();
        }

        public async Task<bool> InsertUsersAsync(List<User> users, List<Role> roles, List<ObjectAccess> objectAccesses,
            List<PropertyAccess> propertyAccesses, List<Permission> permissions)
        {

            return await _repositoryUser.InsertUsersAsync(users, roles, objectAccesses, propertyAccesses, permissions);

        }

        public async Task<List<User>> GetAllUsersAsync()
        {
            return await _repositoryKvAAShardUser.GetAllAsync();
        }


    }
}
