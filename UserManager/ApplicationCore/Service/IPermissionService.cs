﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
   public interface IPermissionService
    {
        Task<List<Permission>> GetPermissionsAsync();
        Task<bool> InsertPermissionAsync(List<Permission> permissions, List<User> users, List<Role> roles);
    }
}
