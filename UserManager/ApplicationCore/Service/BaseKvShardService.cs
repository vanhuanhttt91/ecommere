﻿using Infrastructure.RepositoryOrmlite.UnitOfWork;

namespace ApplicationCore.Service
{
    public class BaseKvShardService
    {
        protected IUnitOfWorkKvShard unitOfWorkKvShard;
        public BaseKvShardService(IUnitOfWorkKvShard unitOfWorkKvShard)
        {
            this.unitOfWorkKvShard = unitOfWorkKvShard;
        }
    }
}
