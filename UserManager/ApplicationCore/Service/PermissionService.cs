﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Domain;
using Infrastructure.Domain;
using Infrastructure.Repository;
using Infrastructure.RepositoryOrmlite;
using Infrastructure.RepositoryOrmlite.UnitOfWork;

namespace ApplicationCore.Service
{
    public class PermissionService : IPermissionService
    {
        private IRepositoryPermission _repositoryPermission;
        private readonly IRepositoryKvAAShard<Permission> _repositoryKvAAShard;
        public PermissionService(IRepositoryPermission repositoryPermission, IUnitOfWorkKvShard unitOfWorkKvShard)
        {
            _repositoryPermission = repositoryPermission;
            _repositoryKvAAShard = unitOfWorkKvShard.GetGenericRepository<Permission>();
        }
        public async Task<List<Permission>> GetPermissionsAsync()
        {
            return await _repositoryKvAAShard.GetAllAsync();
        }
     
        public async Task<bool> InsertPermissionAsync(List<Permission> permissions, List<User> users, List<Role> roles)
        {
            return await _repositoryPermission.InsertPermissionsAsync(permissions, users, roles);
        }
    }
}
