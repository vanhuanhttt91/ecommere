﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Domain;
using Infrastructure.Domain;
using Neo4jClient;
using System.Linq;

namespace Infrastructure.Repository
{
    public class RepositoryPermission : IRepositoryPermission
    {
        private readonly IBoltGraphClient _graphClient;

        public RepositoryPermission(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }

        public Task<List<Permission>> GetPermissionsAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> InsertPermissionsAsync(List<Permission> permissions, List<User> users, List<Role> roles)
        {
            await Task.Run(() =>
            {
                Parallel.ForEach(permissions, async p =>
                {
                    var user = users.Where(x => x.GUId == p.UserId).FirstOrDefault();
                    var role = roles.Where(r => r.Id == p.RoleId).FirstOrDefault();
                    var guid = Guid.NewGuid();
                    var cypherQuery = _graphClient.Cypher.Write
                       .Merge($"(p: {Nodes.Permission} " +
                        $"{{ {nameof(p.Id)}: '{p.Id }', " +
                        $"{nameof(p.BranchId)}: '{p.BranchId}', " +
                        $"{nameof(p.IsDefaultBranch)}: '{p.IsDefaultBranch}'," +
                        $"{nameof(p.RetailerId)}: '{p.RetailerId}'," +
                        $"{nameof(p.RoleId)}: '{p.RoleId}'," +
                        $"{nameof(p.UserId)}: '{p.UserId}'" +
                        $"}})" +
                        $"-[:{Relationships.Permission_User}]->" +
                        $"(u: {Nodes.User} " +
                            $"{{ {nameof(user.GUId)}: '{user.GUId }', " +
                            $"{nameof(user.UserName)}: '{user.UserName}', " +
                            $"{nameof(user.Email)}: '{user.Email}'," +
                            $"{nameof(user.RetailerId)}: '{user.RetailerId}'," +
                            $"{nameof(user.Password)}: '{user.Password}'," +
                            $"{nameof(user.CreatedDate)}: '{user.CreatedDate}'," +
                            $"{nameof(user.IsActive)}: '{user.IsActive}'," +
                            $"{nameof(user.IsAdmin)}: '{user.IsAdmin}'," +
                            $"{nameof(user.CanAccessAnySite)}: '{user.CanAccessAnySite}'," +
                            $"{nameof(user.IsDeleted)}: '{user.IsDeleted}'," +
                            $"{nameof(user.Type)}: '{user.Type}'" +
                            $"}})")
                        .With($"(p)")
                        .Merge($"((p) -[:{Relationships.Permission_Role}]->"+
                            $"(r: {Nodes.Role} " +
                                $"{{ {nameof(role.Id)}: '{role.Id }', " +
                                $"{nameof(role.Name)}: '{role.Name}', " +
                                $"{nameof(role.RetailerId)}: '{role.RetailerId}'," +
                                $"{nameof(role.isActive)}: '{role.isActive}'," +
                                $"{nameof(role.Descriptions)}: '{role.Descriptions}'," +
                                $"{nameof(role.CreatedDate)}: '{role.CreatedDate}'" +
                         $"}}))");
                    await cypherQuery.ExecuteWithoutResultsAsync();
                });
            });
            return true;
        }
    }
}
