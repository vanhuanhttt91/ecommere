﻿using Infrastructure.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public interface IRepositoryRole
    {
        Task<bool> InsertRolesAsync(List<Role> roles);
    }
}
