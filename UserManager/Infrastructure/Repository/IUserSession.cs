﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public interface IUserSession
    {
        Task<List<UserSession>> GetUserSessionsAsync();
        Task<bool> InsertUserSessionsAsync(List<UserSession> userSessions, List<User> users, List<Role> roles);
    }
}
