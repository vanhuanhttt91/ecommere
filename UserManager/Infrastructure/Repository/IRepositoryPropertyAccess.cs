﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
  public  interface IRepositoryPropertyAccess
    {
        Task<List<PropertyAccess>> GetPropertyAccessesAsync();
        Task<bool> InsertPropertyAccessesAsync(List<PropertyAccess> propertyAccesses, List<User> users, List<Role> roles);
    }
}
