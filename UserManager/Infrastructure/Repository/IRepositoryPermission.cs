﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public interface IRepositoryPermission
    {
        Task<bool> InsertPermissionsAsync(List<Permission> permissions, List<User> users, List<Role> roles);
        Task<List<Permission>> GetPermissionsAsync();
    }
}
