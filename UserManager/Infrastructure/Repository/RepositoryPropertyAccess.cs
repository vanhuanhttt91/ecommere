﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Domain;
using Infrastructure.Domain;
using Neo4jClient;

namespace Infrastructure.Repository
{
    public class RepositoryPropertyAccess : IRepositoryPropertyAccess
    {
        private readonly IBoltGraphClient _graphClient;
        public RepositoryPropertyAccess(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public Task<List<PropertyAccess>> GetPropertyAccessesAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> InsertPropertyAccessesAsync(List<PropertyAccess> propertyAccesses, List<User> users, List<Role> roles)
        {

            foreach (var p in propertyAccesses)
            {
                var user = users.Where(x => x.GUId == p.UserId).FirstOrDefault();
                var role = roles.Where(r => r.Id == p.RoleId).FirstOrDefault();
                var cypherQuery = _graphClient.Cypher.Write;

                if (user != null)
                {
                    cypherQuery = cypherQuery.Merge(
                    $"(p: {Nodes.PropertyAccess} " +
                    $"{{ {nameof(p.Id)}: '{p.Id }', " +
                    $"{nameof(p.BranchId)}: '{p.BranchId}', " +
                    $"{nameof(p.Read)}: '{p.Read}'," +
                    $"{nameof(p.RetailerId)}: '{p.RetailerId}'," +
                    $"{nameof(p.RoleId)}: '{p.RoleId}'," +
                    $"{nameof(p.UserId)}: '{p.UserId}'," +
                    $"{nameof(p.Object)}: '{p.Object}'," +
                    $"{nameof(p.Property)}: '{p.Property}'," +
                    $"{nameof(p.Update)}: '{p.Update}'," +
                    $"{nameof(p.Delete)}: '{p.Delete}'," +
                    $"{nameof(p.Create)}: '{p.Create}'" +
                    $"}})" +
                    $"-[:{Relationships.PropertyAccess_User}]->" +
                    $"(u: {Nodes.User} " +
                        $"{{ {nameof(user.GUId)}: '{user.GUId }', " +
                        $"{nameof(user.UserName)}: '{user.UserName}', " +
                        $"{nameof(user.Email)}: '{user.Email}'," +
                        $"{nameof(user.RetailerId)}: '{user.RetailerId}'," +
                        $"{nameof(user.Password)}: '{user.Password}'," +
                        $"{nameof(user.CreatedDate)}: '{user.CreatedDate}'," +
                        $"{nameof(user.IsActive)}: '{user.IsActive}'," +
                        $"{nameof(user.IsAdmin)}: '{user.IsAdmin}'," +
                        $"{nameof(user.CanAccessAnySite)}: '{user.CanAccessAnySite}'," +
                        $"{nameof(user.IsDeleted)}: '{user.IsDeleted}'," +
                        $"{nameof(user.Type)}: '{user.Type}'" +
                        $"}})");
                }
                else
                {
                    cypherQuery = cypherQuery.Merge(
                    $"(p: {Nodes.PropertyAccess} " +
                    $"{{ {nameof(p.Id)}: '{p.Id }', " +
                    $"{nameof(p.BranchId)}: '{p.BranchId}', " +
                    $"{nameof(p.Read)}: '{p.Read}'," +
                    $"{nameof(p.RetailerId)}: '{p.RetailerId}'," +
                    $"{nameof(p.RoleId)}: '{p.RoleId}'," +
                    $"{nameof(p.UserId)}: '{p.UserId}'," +
                    $"{nameof(p.Object)}: '{p.Object}'," +
                    $"{nameof(p.Property)}: '{p.Property}'," +
                    $"{nameof(p.Update)}: '{p.Update}'," +
                    $"{nameof(p.Delete)}: '{p.Delete}'," +
                    $"{nameof(p.Create)}: '{p.Create}'" +
                    $"}})");
                }
                if (role != null)
                {
                    cypherQuery = cypherQuery.With($"(p)")
                    .Merge($"((p) -[:{Relationships.PropertyAccess_Role}]->" +
                        $"(r: {Nodes.Role} " +
                            $"{{ {nameof(role.Id)}: '{role.Id }', " +
                            $"{nameof(role.Name)}: '{role.Name}', " +
                            $"{nameof(role.RetailerId)}: '{role.RetailerId}'," +
                            $"{nameof(role.isActive)}: '{role.isActive}'," +
                            $"{nameof(role.Descriptions)}: '{role.Descriptions}'," +
                            $"{nameof(role.CreatedDate)}: '{role.CreatedDate}'" +
                     $"}}))");
                }

                await cypherQuery.ExecuteWithoutResultsAsync();
            };

            return true;

        }
    }
}
