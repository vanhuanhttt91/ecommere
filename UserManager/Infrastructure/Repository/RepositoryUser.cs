﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class RepositoryUser : IRepositoryUser
    {
        private readonly IBoltGraphClient _graphClient;

        public RepositoryUser(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public async Task<bool> InsertUsersAsync(List<User> users, List<Role> roles, List<ObjectAccess> objectAccesses,
            List<PropertyAccess> propertyAccesses, List<Permission> permissions)
        {
            try
            {
                await Task.Run(() =>
                {
                    Parallel.ForEach(users, async user =>
                    {
                        var permissionList = permissions.Where(r => r.UserId == user.GUId).ToList();
                        var objectAccessList = objectAccesses.Where(r => r.UserId == user.GUId).ToList();
                        var propertyAccessList = propertyAccesses.Where(r => r.UserId == user.GUId).ToList();
                        foreach (var permission in permissionList)
                        {

                            var role = roles.Where(r => r.Id == permission.RoleId).FirstOrDefault();
                            var cypherQuery = _graphClient.Cypher.Write;
                            cypherQuery = cypherQuery.Merge($"(u: {Nodes.User} " +
                                  $"{{ {nameof(user.GUId)}: '{user.GUId }', " +
                                  $"{nameof(user.UserName)}: '{user.UserName}', " +
                                  $"{nameof(user.Email)}: '{user.Email}'," +
                                  $"{nameof(user.RetailerId)}: '{user.RetailerId}'," +
                                  $"{nameof(user.Password)}: '{user.Password}'," +
                                  $"{nameof(user.CreatedDate)}: '{user.CreatedDate}'," +
                                  $"{nameof(user.IsActive)}: '{user.IsActive}'," +
                                  $"{nameof(user.IsAdmin)}: '{user.IsAdmin}'," +
                                  $"{nameof(user.CanAccessAnySite)}: '{user.CanAccessAnySite}'," +
                                  $"{nameof(user.IsDeleted)}: '{user.IsDeleted}'," +
                                  $"{nameof(user.Type)}: '{user.Type}'" +
                                  $"}})");
                            cypherQuery = cypherQuery.Merge(
                                         $"(r{permission.Id}: {Nodes.Role} " +
                                         $"{{ {nameof(role.Id)}: '{role.Id }', " +
                                         $"{nameof(role.Name)}: '{role.Name}', " +
                                         $"{nameof(role.RetailerId)}: '{role.RetailerId}'," +
                                         $"{nameof(role.isActive)}: '{role.isActive}'," +
                                         $"{nameof(role.Descriptions)}: '{role.Descriptions}'," +
                                         $"{nameof(role.CreatedDate)}: '{role.CreatedDate}'" +
                                         $"}})");
                            cypherQuery = cypherQuery.Merge($"(u)-[rp:{Relationships.Permission_Role}]->(r{permission.Id})")
                            .Set($" rp.{nameof(permission.Id)} = '{permission.Id }', " +
                                    $"rp.{nameof(permission.BranchId)} = '{permission.BranchId}', " +
                                    $"rp.{nameof(permission.IsDefaultBranch)} = '{permission.IsDefaultBranch}'," +
                                    $"rp.{nameof(permission.RetailerId)} = '{permission.RetailerId}'," +
                                    $"rp.{nameof(permission.RoleId)} = '{permission.RoleId}'," +
                                    $"rp.{nameof(permission.UserId)} = '{permission.UserId}'"
                                  );
                            await cypherQuery.ExecuteWithoutResultsAsync();
                        }
                        foreach (var propertyAccess in propertyAccessList)
                        {

                            var role = roles.Where(r => r.Id == propertyAccess.RoleId).FirstOrDefault();
                            if (role == null) continue;
                            var cypherQuery = _graphClient.Cypher.Write;
                            cypherQuery = cypherQuery.Merge($"(u: {Nodes.User} " +
                                  $"{{ {nameof(user.GUId)}: '{user.GUId }', " +
                                  $"{nameof(user.UserName)}: '{user.UserName}', " +
                                  $"{nameof(user.Email)}: '{user.Email}'," +
                                  $"{nameof(user.RetailerId)}: '{user.RetailerId}'," +
                                  $"{nameof(user.Password)}: '{user.Password}'," +
                                  $"{nameof(user.CreatedDate)}: '{user.CreatedDate}'," +
                                  $"{nameof(user.IsActive)}: '{user.IsActive}'," +
                                  $"{nameof(user.IsAdmin)}: '{user.IsAdmin}'," +
                                  $"{nameof(user.CanAccessAnySite)}: '{user.CanAccessAnySite}'," +
                                  $"{nameof(user.IsDeleted)}: '{user.IsDeleted}'," +
                                  $"{nameof(user.Type)}: '{user.Type}'" +
                                  $"}})");
                            cypherQuery = cypherQuery.Merge(
                                         $"(r{propertyAccess.Id}: {Nodes.Role} " +
                                         $"{{ {nameof(role.Id)}: '{role.Id }', " +
                                         $"{nameof(role.Name)}: '{role.Name}', " +
                                         $"{nameof(role.RetailerId)}: '{role.RetailerId}'," +
                                         $"{nameof(role.isActive)}: '{role.isActive}'," +
                                         $"{nameof(role.Descriptions)}: '{role.Descriptions}'," +
                                         $"{nameof(role.CreatedDate)}: '{role.CreatedDate}'" +
                                         $"}})");
                            cypherQuery = cypherQuery.Merge($"(u)-[rp:{Relationships.Permission_Role}]->(r{propertyAccess.Id})")
                            .Set($" " +
                                $"rp.{nameof(propertyAccess.Id)} = '{propertyAccess.Id }', " +
                                $"rp.{nameof(propertyAccess.BranchId)} = '{propertyAccess.BranchId}', " +
                                $"rp.{nameof(propertyAccess.Read)} ='{propertyAccess.Read}'," +
                                $"rp.{nameof(propertyAccess.RetailerId)} = '{propertyAccess.RetailerId}'," +
                                $"rp.{nameof(propertyAccess.RoleId)} = '{propertyAccess.RoleId}'," +
                                $"rp.{nameof(propertyAccess.UserId)} = '{propertyAccess.UserId}'," +
                                $"rp.{nameof(propertyAccess.Object)} = '{propertyAccess.Object}'," +
                                $"rp.{nameof(propertyAccess.Property)} = '{propertyAccess.Property}'," +
                                $"rp.{nameof(propertyAccess.Update)} ='{propertyAccess.Update}'," +
                                $"rp.{nameof(propertyAccess.Delete)} = '{propertyAccess.Delete}'," +
                                $"rp.{nameof(propertyAccess.Create)} = '{propertyAccess.Create}'"
                                  );
                            await cypherQuery.ExecuteWithoutResultsAsync();
                        }
                        foreach (var objectAccess in objectAccessList)
                        {
                            var role = roles.Where(r => r.Id == objectAccess.RoleId).FirstOrDefault();
                            if (role == null) continue;
                            var cypherQuery = _graphClient.Cypher.Write;
                            cypherQuery = cypherQuery.Merge($"(u: {Nodes.User} " +
                                  $"{{ {nameof(user.GUId)}: '{user.GUId }', " +
                                  $"{nameof(user.UserName)}: '{user.UserName}', " +
                                  $"{nameof(user.Email)}: '{user.Email}'," +
                                  $"{nameof(user.RetailerId)}: '{user.RetailerId}'," +
                                  $"{nameof(user.Password)}: '{user.Password}'," +
                                  $"{nameof(user.CreatedDate)}: '{user.CreatedDate}'," +
                                  $"{nameof(user.IsActive)}: '{user.IsActive}'," +
                                  $"{nameof(user.IsAdmin)}: '{user.IsAdmin}'," +
                                  $"{nameof(user.CanAccessAnySite)}: '{user.CanAccessAnySite}'," +
                                  $"{nameof(user.IsDeleted)}: '{user.IsDeleted}'," +
                                  $"{nameof(user.Type)}: '{user.Type}'" +
                                  $"}})");
                            if (role != null)
                            {
                                cypherQuery = cypherQuery.Merge(
                                        $"(r{objectAccess.Id}: {Nodes.Role} " +
                                        $"{{ {nameof(role.Id)}: '{role.Id }', " +
                                        $"{nameof(role.Name)}: '{role.Name}', " +
                                        $"{nameof(role.RetailerId)}: '{role.RetailerId}'," +
                                        $"{nameof(role.isActive)}: '{role.isActive}'," +
                                        $"{nameof(role.Descriptions)}: '{role.Descriptions}'," +
                                        $"{nameof(role.CreatedDate)}: '{role.CreatedDate}'" +
                                        $"}})");
                                cypherQuery = cypherQuery.Merge($"(u)-[rp:{Relationships.Permission_Role}]->(r{objectAccess.Id})")
                                  .Set($"rp.{nameof(objectAccess.Id)} = '{objectAccess.Id }', " +
                                      $"rp.{nameof(objectAccess.BranchId)} = '{objectAccess.BranchId}', " +
                                      $"rp.{nameof(objectAccess.Create)} = '{objectAccess.Create}'," +
                                      $"rp.{nameof(objectAccess.RetailerId)}: '{objectAccess.RetailerId}'," +
                                      $"rp.{nameof(objectAccess.RoleId)} = '{objectAccess.RoleId}'," +
                                      $"rp.{nameof(objectAccess.Object)} = '{objectAccess.Object}'," +
                                      $"rp.{nameof(objectAccess.Update)} = '{objectAccess.Update}'," +
                                      $"rp.{nameof(objectAccess.UserId)} = '{objectAccess.UserId}'"
                                  );
                            }


                            await cypherQuery.ExecuteWithoutResultsAsync();
                        }
                    });
                });
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

    }
}
