﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
   public interface IRepositoryObjectAccess
    {
        Task<List<ObjectAccess>> GetObjectAccessesAsync();
        Task<bool> InsertObjectAccessAsync(List<ObjectAccess> objectAccesses, List<User> users, List<Role> roles);
    }
}
