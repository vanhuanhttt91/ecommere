﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public interface IRepositoryUser
    {
        Task<bool> InsertUsersAsync(List<User> users, List<Role> roles, List<ObjectAccess> objectAccesses,
            List<PropertyAccess> propertyAccesses, List<Permission> permissions);
    }
}
