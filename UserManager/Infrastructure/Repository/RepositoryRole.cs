﻿using ApplicationCore.Domain;
using Infrastructure.Domain;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class RepositoryRole : IRepositoryRole
    {
        private readonly IBoltGraphClient _graphClient;

        public RepositoryRole(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public async Task<bool> InsertRolesAsync(List<Role> roles)
        {
            try
            {
                await Task.Run(() =>
                {
                    Parallel.ForEach(roles, async role =>
                    {
                        var guid = Guid.NewGuid();
                        var cypherQuery = _graphClient.Cypher.Write.Merge(
                            $"(r: {Nodes.Role} " +
                            $"{{ {nameof(role.Id)}: '{role.Id }', " +
                            $"{nameof(role.Name)}: '{role.Name}', " +
                            $"{nameof(role.RetailerId)}: '{role.RetailerId}'," +
                            $"{nameof(role.isActive)}: '{role.isActive}'," +
                            $"{nameof(role.Descriptions)}: '{role.Descriptions}'," +
                            $"{nameof(role.CreatedDate)}: '{role.CreatedDate}'" +
                            $"}})");
                        await cypherQuery.ExecuteWithoutResultsAsync();
                    });
                });
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
