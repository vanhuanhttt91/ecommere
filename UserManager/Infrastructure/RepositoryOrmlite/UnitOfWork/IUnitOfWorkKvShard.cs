﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.RepositoryOrmlite.UnitOfWork
{
    public interface IUnitOfWorkKvShard : IDisposable
    {
        IRepositoryKvAAShard<T> GetGenericRepository<T>() where T : class;

        void Commit();
        void Rollback();

    }
}
