﻿using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Data;

namespace Infrastructure.RepositoryOrmlite.UnitOfWork
{
    public class UnitOfWorkKvShard : IUnitOfWorkKvShard
    {
        protected IDbConnection _connectionKvShard;
        protected IDbTransaction _transactionKvShard;
        private bool _disposed;

        public UnitOfWorkKvShard(IDbConnectionFactory dbConnection)
        {
            _connectionKvShard = dbConnection.OpenDbConnection();
            _transactionKvShard = _connectionKvShard.OpenTransaction(IsolationLevel.ReadUncommitted);
        }

        public IRepositoryKvAAShard<T> GetGenericRepository<T>() where T : class
        {
            return new RepositoryKvShard<T>(_connectionKvShard);
        }


        public void Commit()
        {
            try
            {
                _transactionKvShard.Commit();
            }
            catch
            {
                _transactionKvShard.Rollback();
                throw;
            }
            finally
            {
                _transactionKvShard.Dispose();
            }
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transactionKvShard != null)
                    {
                        _transactionKvShard.Dispose();
                        _transactionKvShard = null;
                    }
                    if (_connectionKvShard != null)
                    {
                        _connectionKvShard.Dispose();
                        _connectionKvShard = null;
                    }
                }
                _disposed = true;
            }
        }

        public void Rollback()
        {
            _transactionKvShard.Rollback();
        }
    }
}
