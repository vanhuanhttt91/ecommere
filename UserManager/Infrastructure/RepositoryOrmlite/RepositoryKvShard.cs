﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.RepositoryOrmlite
{
    public class RepositoryKvShard<T> : RepositoryBaseKvShard1, IRepositoryKvAAShard<T> where T : class
    {
        public RepositoryKvShard(IDbConnection connection) : base(connection)
        {
        }

        public virtual SqlExpression<T> CreateQuery()
        {
            var sqlExpression = ConnectionKvShard.From<T>();

            return sqlExpression;
        }

        public virtual async Task<List<T>> GetAllAsync()
        {
            return await ConnectionKvShard.SelectAsync<T>();
        }

        public virtual async Task<List<T>> GetAsync(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null)
        {
            return await GetAsync<T>(exp, skip, take, orderby, orderbydesc);
        }

        public virtual async Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null)
        {
            var query = exp.Skip(skip).Take(take);

            if (orderby?.Any() == true)
            {
                query = query.OrderByFields(orderby);
            }

            if (orderbydesc?.Any() == true)
            {
                query = query.OrderByFieldsDescending(orderbydesc);
            }

            return await ConnectionKvShard.SelectAsync<TResult>(query);
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            var o = await ConnectionKvShard.SingleByIdAsync<T>(id);

            return o;
        }

        public virtual async Task InsertAsync(T o)
        {
            await ConnectionKvShard.InsertAsync(o);
        }
        public virtual async Task<long> InsertAsync(T o, bool selectIdentity)
        {
            return await ConnectionKvShard.InsertAsync(o, selectIdentity);
        }

        public virtual async Task UpdateAsync(T o, Expression<Func<T, object>> onlyFields = null, Expression<Func<T, bool>> where = null)
        {
            await ConnectionKvShard.UpdateOnlyAsync(o, onlyFields, where);
        }

        public virtual async Task<bool> SaveAsync(T o)
        {
            return await ConnectionKvShard.SaveAsync(o);
        }

        public virtual async Task DeleteAsync(int id)
        {
            await ConnectionKvShard.DeleteByIdAsync<T>(id);
        }

        public virtual async Task DeleteAsync(Expression<Func<T, bool>> exp)
        {
            await ConnectionKvShard.DeleteAsync(exp);
        }

        public virtual async Task<long> CountAsync(SqlExpression<T> exp)
        {

            return await ConnectionKvShard.CountAsync(exp);
        }

        public virtual void CreateTableIfNotExists()
        {
            ConnectionKvShard.CreateTableIfNotExists<T>();
        }

        public virtual async Task InsertAllAsync(IEnumerable<T> o)
        {
            await ConnectionKvShard.InsertAllAsync(o);
        }
    }

    public class RepositoryBaseKvShard1
    {
        protected IDbConnection ConnectionKvShard;

        public RepositoryBaseKvShard1(IDbConnection connection)
        {
            ConnectionKvShard = connection;
        }
    }
}
