﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.RepositoryOrmlite
{
    public interface IRepositoryKvAAShard<T> where T : class
    {
        SqlExpression<T> CreateQuery();

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetAsync(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null);

        Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null);

        Task<T> GetByIdAsync(int id);

        Task InsertAsync(T o);
        Task<long> InsertAsync(T o, bool selectIdentity);

        Task UpdateAsync(T o, Expression<Func<T, object>> onlyFields = null, Expression<Func<T, bool>> where = null);

        Task<bool> SaveAsync(T o);

        Task DeleteAsync(int id);

        Task DeleteAsync(Expression<Func<T, bool>> exp);

        Task<long> CountAsync(SqlExpression<T> exp);
        Task InsertAllAsync(IEnumerable<T> o);
        void CreateTableIfNotExists();
    }
}
