﻿namespace ApplicationCore.Domain
{
    public class Nodes
    {
        public const string User = "User";
        public const string UserSession = "UserSession";
        public const string ObjectAccess = "ObjectAccess";
        public const string Permission = "Permission";
        public const string PropertyAccess = "PropertyAccess";
        public const string Role = "Role";
    }
}
