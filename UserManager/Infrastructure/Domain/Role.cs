﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain
{
   public class Role
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public int RetailerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool isActive { get; set; }
    }
}
