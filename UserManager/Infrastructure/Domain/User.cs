﻿using System;

namespace ApplicationCore.Domain
{
    public class User
    {
        public Guid GUId { get; set; }
        public string Email { get; set; }

        public int RetailerId { get; set; }
        public string UserName { get; set; }

        public string Password { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsAdmin { get; set; }

        public bool CanAccessAnySite { get; set; }

        public bool? IsDeleted { get; set; }

        public byte Type { get; set; }
    }
}