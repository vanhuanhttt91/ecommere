﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain
{
    public class Permission
    {
        public int Id { get; set; }

        public Guid UserId { get; set; }
        public int BranchId { get; set; }
        public long RoleId { get; set; }
        public int RetailerId { get; set; }
        public bool? IsDefaultBranch { get; set; }
    }
}
