﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain
{
    public class ObjectAccess
    {
        public long Id { get; set; }

        public long? RoleId { get; set; }
        public string Object { get; set; }
        public bool Read { get; set; }
        public bool Create { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public int RetailerId { get; set; }

        public Guid? UserId { get; set; }

        public int? BranchId { get; set; }
    }
}
