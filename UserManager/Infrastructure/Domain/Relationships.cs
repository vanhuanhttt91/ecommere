﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain
{
    public class Relationships
    {
        public const string Permission_User = "Permission_User";
        public const string ObjectAccess_User = "ObjectAccess_User";
        public const string PropertyAccess_User = "PropertyAccess_User";
        public const string ObjectAccess_Role = "ObjectAccess_Role";
        public const string PropertyAccess_Role = "PropertyAccess_Role";
        public const string Permission_Role = "Permission_Role";
    }
}
