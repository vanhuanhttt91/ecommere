﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Domain
{
   public class UserSession
    {
        public int Id { get; set; }
        
        public Guid UserId { get; set; }
        
        public string UserName { get; set; }
      
        public string KvSessionId { get; set; }
 
        public int RetailerId { get; set; }
       
        public DateTime CreatedDate { get; set; }
        
        public string ClientIp { get; set; }
    }
}
