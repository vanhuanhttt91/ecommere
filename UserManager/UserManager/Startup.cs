using ApplicationCore.Domain;
using ApplicationCore.Service;
using AutoMapper;
using Infrastructure.Repository;
using Infrastructure.RepositoryOrmlite.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Neo4j.Driver.V1;
using Neo4jClient;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using System.Reflection;

namespace UserManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.Configure<Neo4jSetting>(Configuration.GetSection("Neo4jSetting"));
            services.AddAutoMapper(Assembly.GetAssembly(typeof(MappingConfiguration)));
            services.AddScoped<IUnitOfWorkKvShard, UnitOfWorkKvShard>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IPermissionService, PermissionService>();
            services.AddScoped<IPropertyAccessService, PropertyAccessService>();
            services.AddScoped<IObjectAccessService, ObjectAccessService>();

            services.AddScoped<IRepositoryUser, RepositoryUser>();
            services.AddScoped<IRepositoryRole, RepositoryRole>();
            services.AddScoped<IRepositoryPermission, RepositoryPermission>();
            services.AddScoped<IRepositoryPropertyAccess, RepositoryPropertyAccess>();
            services.AddScoped<IRepositoryObjectAccess, RepositoryObjectAccess>();

            // RegisterJWTAuthentication(services);
            services.AddMvc();
            RegisterNeo4jDriver(services);
            RegisterGraphClient(services);
            RegisterOrmLite(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private IServiceCollection RegisterNeo4jDriver(IServiceCollection services)
        {
            services.AddScoped(typeof(IDriver), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4jSetting").Get<Neo4jSetting>();
                var authToken = AuthTokens.Basic(neo4jSetting.UserName, neo4jSetting.Password);
                var config = new Config();
                var driver = GraphDatabase.Driver(neo4jSetting.BoltUrl, authToken, config);
                return driver;
            });

            return services;
        }

        private IServiceCollection RegisterGraphClient(IServiceCollection services)
        {
            services.AddScoped(typeof(IBoltGraphClient), resolver =>
            {
                var client = new BoltGraphClient(services.BuildServiceProvider().GetService<IDriver>());

                if (!client.IsConnected)
                {
                    client.Connect();
                }

                return client;
            });

            return services;
        }

        private IServiceCollection RegisterOrmLite(IServiceCollection services)
        {

            services.AddScoped<IDbConnectionFactory>(provider => {
                var factory = new OrmLiteConnectionFactory(Configuration.GetConnectionString("sqlConnection"), new SqlServer2017OrmLiteDialectProvider());
                factory.OpenDbConnection();
                return factory;
            });
            
            return services;
        }

    }
}
