﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Domain;
using ApplicationCore.Service;
using Infrastructure.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UserManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagerController : ControllerBase
    {
        private IUserService _userServive;
        private IRoleService _roleService;
        private IPermissionService _permissionService;
        private IPropertyAccessService _propertyAccessService;
        private IObjectAccessService _objectAccessService;


        public UserManagerController(IUserService userServive, IRoleService roleService, IPermissionService permissionService, IPropertyAccessService propertyAccessService, IObjectAccessService objectAccessService)
        {
            this._userServive = userServive;
            this._roleService = roleService;
            _permissionService = permissionService;
            _propertyAccessService = propertyAccessService;
            _objectAccessService = objectAccessService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get()
        {

            
            var users = await _userServive.GetAllUsersAsync();
            var roles = await _roleService.GetAllRolesAsync();
            var permissions = await _permissionService.GetPermissionsAsync();
            var objectAccesses = await _objectAccessService.GetObjectAccessesAsync();
            var propertyAccesses = await _propertyAccessService.GetPropertyAccessesAsync();
            await _userServive.InsertUsersAsync(users, roles, objectAccesses, propertyAccesses, permissions);
            //await _roleService.InsertRoleAsync(roles);
          //  await _permissionService.InsertPermissionAsync(permisses, users, roles);
          // await _objectAccessService.InsertObjectAccessesAsync(objectAcccesses, users, roles);
        //    await _propertyAccessService.InsertPropertyAccessesAsync(propertyAccesses, users, roles);
            return new string[] { string.Join(",", "0") };
        }
    }

}